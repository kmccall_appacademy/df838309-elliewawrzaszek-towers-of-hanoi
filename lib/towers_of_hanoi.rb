class TowersOfHanoi

  attr_accessor :towers

  def initialize
    @towers = Array.new([[3, 2, 1],[],[]])
  end

  def play
    puts render

    until won?
      from_tower = get_from_tower
      to_tower = get_to_tower

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        display
      else
        display
        puts "Try a valid move."
      end

    end
    puts "You are a true champion!"
  end

  def move(from_tower, to_tower)
    until valid_move?(from_tower, to_tower)
      from_tower = get_from_tower
      to_tower = get_to_tower
    end

    disc = @towers[from_tower].pop
    @towers[to_tower] << disc
  end

  def valid_move?(from_tower, to_tower)
    to = @towers[to_tower]
    from = @towers[from_tower]
    return true unless from.empty? || (!to.empty? && to.last < from.last)

  end

  def won?
    return true if @towers[1].size == 3 || @towers[2].size == 3
  end

  def get_from_tower
    puts "Which tower would you like to take a disc from?"
    gets.chomp.to_i
  end

  def get_to_tower
    puts "Where do you want to place a disc?"
    gets.chomp.to_i
  end

  def render
    'Tower 0:   ' + @towers[0].join('   ') + "\n" +
    'Tower 1:   ' + @towers[1].join('   ') + "\n" +
    'Tower 2:   ' + @towers[2].join('   ') + "\n"
  end

  def display
    system("clear")
    puts render
  end

end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new.play

end

# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
